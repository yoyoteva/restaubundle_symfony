<?php

namespace App\Controller;

use App\Entity\Dish;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class BlockController extends AbstractController
{
    public function dayDishesAction($max = 3)
    {
        $dishes = $this->getDoctrine()->getRepository(Dish::class)->getLastStickyPlats($max);
        return $this->render(
            'Partials/day_dishes.html.twig',
            array('dishes' => $dishes)
        );
    }
}
