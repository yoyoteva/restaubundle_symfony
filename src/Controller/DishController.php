<?php

namespace App\Controller;

use App\Entity\Dish;
use App\Form\DishType;
use App\Repository\DishRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/dish")
 */
class DishController extends AbstractController
{
    /**
     * @Route("/", name="dish_index", methods="GET")
     */
    public function index(DishRepository $dishRepository): Response
    {
        return $this->render('dish/index.html.twig', ['dishes' => $dishRepository->findAll()]);
    }

    /**
     * @Route("/new", name="dish_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $dish = new Dish();
        $form = $this->createForm(DishType::class, $dish);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($dish);
            $em->flush();

            $this->addFlash('notice',
                'Le plat '.$dish->getName().' a bien été crée');


            return $this->redirectToRoute('dish_index');
        }

        return $this->render('dish/new.html.twig', [
            'dish' => $dish,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="dish_show", methods="GET")
     */
    public function show(Dish $dish): Response
    {
        return $this->render('dish/show.html.twig', ['dish' => $dish]);
    }

    /**
     * @Route("/{id}/edit", name="dish_edit", methods="GET|POST")
     */
    public function edit(Request $request, Dish $dish): Response
    {
        $form = $this->createForm(DishType::class, $dish);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('notice',
                'Le plat '.$dish->getName().' a bien été mis à jour');

            return $this->redirectToRoute('dish_index', ['id' => $dish->getId()]);
        }

        return $this->render('dish/edit.html.twig', [
            'dish' => $dish,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="dish_delete", methods="DELETE")
     */
    public function delete(Request $request, Dish $dish): Response
    {
        if ($this->isCsrfTokenValid('delete'.$dish->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($dish);
            $em->flush();

            $this->addFlash('notice',
                'Le plat '.$dish->getName().' a bien été supprimé');

        }

        return $this->redirectToRoute('dish_index');
    }
}
