<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index()
    {
        return $this->render('admin/index.html.twig', [
            'controller_name' => 'AdminController',
            "page_title" => "Panneau d'administration"
        ]);
    }

    /**
     * @Route("/admin/team/inserer", name="admin_team_insert", methods = {"GET"})
     */
    public function admin_team_insert(Request $request)
    {
        $user = new User();

        $form = $this->createFormBuilder($user)
            ->add('username', TextType::class)
            ->add('firstname', TextType::class)
            ->add('lastname', TextType::class)
            ->add('email', TextType::class)
            ->add('jobtitle', TextType::class)
            ->add('submit', SubmitType::class, array('label' => 'Valider'))
            ->setMethod('GET')
            ->setAction($this->generateUrl('admin_team_insert'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('front_team');
        }

        return $this->render('front/form_insert.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/admin/dishes/insert", name="admin_dish_insert", methods = {"GET"})
     */
    public function insertDishesFromJSON(){
        $path = '../dishes.json';

        $em = $this->getDoctrine()->getManager();
        $json = file_get_contents($path);

        $datas = json_decode($json);

        $desserts = $datas->{"desserts"};

        foreach ($desserts as $dessert){
            $dish = new Dish();
            $category = $this->getDoctrine()->getRepository(Category::class)->find(5);
            $dish->setCategory($category);
            $dish->setName($dessert->{"name"});
            $dish->setCalories($dessert->{"calories"});
            $dish->setPrice(floatval($dessert->{"price"}));
            $dish->setUmage($dessert->{"image"});
            $dish->setDescription($dessert->{"text"});
            $dish->setSticky($dessert->{"sticky"});

            foreach ($dessert->allergens as $allergen){
                $allergenExists = $this->getDoctrine()->getRepository(Allergen::class)->findOneBy(array('name' => $allergen));
                if (!$allergenExists){
                    $allergenExists = new Allergen();
                    $allergenExists->setName($allergen);
                }
                $dish->addAllergen($allergenExists);
                $em->persist($dish);
            }
            $em->persist($dish);
        }

        $entrees = $datas->{"entrees"};

        foreach ($entrees as $entree){
            $dish = new Dish();
            $category = $this->getDoctrine()->getRepository(Category::class)->find(3);
            $dish->setCategory($category);
            $dish->setName($entree->{"name"});
            $dish->setCalories($entree->{"calories"});
            $dish->setPrice(floatval($entree->{"price"}));
            $dish->setUmage($entree->{"image"});
            $dish->setDescription($entree->{"text"});
            $dish->setSticky($entree->{"sticky"});

            foreach ($entree->allergens as $allergen){
                $allergenExists = $this->getDoctrine()->getRepository(Allergen::class)->findOneBy(array('name' => $allergen));
                if (!$allergenExists){
                    $allergenExists = new Allergen();
                    $allergenExists->setName($allergen);
                }
                $dish->addAllergen($allergenExists);
                $em->persist($dish);
            }
            $em->persist($dish);
        }

        $plats = $datas->{"plats"};
        foreach ($plats as $plat){
            $dish = new Dish();
            $category = $this->getDoctrine()->getRepository(Category::class)->find(4);
            $dish->setCategory($category);
            $dish->setName($plat->{"name"});
            $dish->setCalories($plat->{"calories"});
            $dish->setPrice(floatval($plat->{"price"}));
            $dish->setUmage($plat->{"image"});
            $dish->setDescription($plat->{"text"});
            $dish->setSticky($plat->{"sticky"});

            foreach ($plat->allergens as $allergen){
                $allergenExists = $this->getDoctrine()->getRepository(Allergen::class)->findOneBy(array('name' => $allergen));
                if (!$allergenExists){
                    $allergenExists = new Allergen();
                    $allergenExists->setName($allergen);
                }
                $dish->addAllergen($allergenExists);
                $em->persist($dish);
            }
            $em->persist($dish);
        }
        $em->flush();

        return $this->redirectToRoute('front_dishes');
    }
}
