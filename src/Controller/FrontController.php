<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Dish;
use Doctrine\DBAL\Exception\DatabaseObjectNotFoundException;
use Doctrine\DBAL\Exception\DriverException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

use App\Entity\User;

use App\Service\RandomSlogan;

class FrontController extends Controller
{
    /**
     * @Route("/", name="front_home")
     */
    public function index()
    {
        return $this->render('front/index.html.twig', [
            'controller_name' => 'FrontController',
            "page_title" => "Bienvenue"
        ]);
    }

    /**
     * @Route("/equipe", name="front_team", methods = {"GET"})
     */
    public function equipe(RandomSlogan $rdmsl)
    {
        $rhService = $this->get('restau.services.rh');

        $team = $rhService->getDayTeam(\date('Y-m-d'));

        if (!$team) {
            throw $this->createNotFoundException(
                'No user found'
            );
        }

        return $this->render('front/equipe.html.twig', [
            'controller_name' => 'FrontController',
            'team' => $team,
            'slogan' => $rdmsl->getSlogan(),
            "page_title" => "Notre equipe"
        ]);
    }

    /**
     * @Route("/equipe/insert", name="insert_form", methods = {"GET"})
     */
    public function insert_user()
    {
        $form = $this->createFormBuilder()
            ->add('username', TextType::class)
            ->add('firstname', TextType::class)
            ->add('lastname', TextType::class)
            ->add('email', TextType::class)
            ->add('jobtitle', TextType::class)
            ->add('submit', SubmitType::class, array('label' => 'Valider'))
            ->setMethod('GET')
            ->setAction($this->generateUrl('admin_team_insert'))
            ->getForm();

        return $this->render('front/form_insert.html.twig', array(
            'form' => $form->createView(),
            "page_title" => "Créer un utilisateur"
        ));
    }

    /**
     * @Route("/carte", name="front_dishes", methods = {"GET"})
     */
    public function carte()
    {
        $categories = $this->getDoctrine()->getRepository(Category::class)->getCategoriesWithNbDishes();

        return $this->render('front/front_carte.html.twig', array(
            'categories' => $categories,
            "page_title" => "Carte du restaurant"
        ));

    }

    /**
     * @Route("/carte/{id_category}", name="front_dishes_category", methods = {"GET"})
     */
    public function front_dishes_category($id_category)
    {
        $category = $this->getDoctrine()->getRepository(Category::class)->find($id_category);

        if(!empty($category)){
            $dishes = $this->getDoctrine()->getRepository(Dish::class)->findBy([
                "Category" => $category
            ]);

            return $this->render('front/front_carte_category.html.twig', array(
                'dishes' => $dishes,
                "cat" => $category,
                "page_title" => "Carte des ".$category->getName()
            ));
        }else{
            throw $this->createNotFoundException("Pas de catégorie correspondante.");
        }

    }

    /**
     * @Route("/mentions-legales", name="front_legals", methods={"GET"})
     */
    public function mentions()
    {
        return $this->render('front/mentions.html.twig', [
            "page_title" => "Mentions légales"
        ]);
    }

}
