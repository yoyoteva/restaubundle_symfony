<?php
/**
 * Created by PhpStorm.
 * User: joffr
 * Date: 30/01/2019
 * Time: 17:08
 */

namespace App\EventListener;

use App\Entity\Order;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Bundle\TwigBundle\DependencyInjection\TwigExtension;

class EventListenerMail
{
    private $mailer;
    private $templating;

    public function __construct(\Swift_Mailer $mailer,TwigExtension $templating)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if (!$entity instanceof Order) {
            return;
        }

        $message = (new \Swift_Message('NOUVELLE COMMANDE'))
            ->setFrom('yoyoteva@gmail.com')
            ->setTo('yohann.bethoule@etu.unilim.fr')
            ->setBody(
                $this->templating->render('emails/newCommande.html.twig',[
                    'plats' => $entity->getPlats(),
                    'table' => $entity->getTableOrder()
                ]),
                'text/html'
            );

        $this->mailer->send($message);
    }
}
