<?php
/**
 * Created by PhpStorm.
 * User: joffr
 * Date: 02/02/2019
 * Time: 11:05
 */

namespace App\EventListener;

use App\Entity\Order;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use App\Event\OrderPayedEvent;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Templating\EngineInterface;

class EventListenerCommande
{
    private $mailer;
    private $templating;
    private $dispatcher;

    public function __construct(\Swift_Mailer $mailer,TwigExtension $templating,EventDispatcher $dispatcher)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->dispatcher = $dispatcher;
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if (!$entity instanceof Order) {
            return;
        }

        if ($entity->getStatus() === "PRETE"){
            $message = (new \Swift_Message('COMMANDE PRETE'))
                ->setFrom('yoyoteva@gmail.com')
                ->setTo('yohann.bethoule@etu.unilim.fr')->setBody(
                $this->templating->render('emails/commandePrete.html.twig',[
                    "num_commande" => $entity->getId(),
                    'plats' => $entity->getPlats(),
                    'table' => $entity->getTableOrder()
                ]),
                'text/html'
            );
            $this->mailer->send($message);
        }

        if ($entity->getStatus() === "SERVIE"){
            $message = (new \Swift_Message('COMMANDE SERVIE'))
                ->setFrom('yoyoteva@gmail.com')
                ->setTo('yohann.bethoule@etu.unilim.fr')->setBody(
                    $this->templating->render('emails/commandeServie.html.twig',[
                        "num_commande" => $entity->getId(),
                        'plats' => $entity->getPlats(),
                        'table' => $entity->getTableOrder()
                    ]),
                    'text/html'
                );
            $this->mailer->send($message);
        }

        if ($entity->getStatus() === "PAYEE"){
            $message = (new \Swift_Message('COMMANDE PAYEE'))
            ->setFrom('yoyoteva@gmail.com')
            ->setTo('yohann.bethoule@etu.unilim.fr')->setBody(
                $this->templating->render('emails/commandePayee.html.twig',[
                    "num_commande" => $entity->getId(),
                    'plats' => $entity->getPlats(),
                    'table' => $entity->getTableOrder()
                ]),
                'text/html'
            );
            $this->mailer->send($message);

            $event = new OrderPayedEvent($entity);

            $this->dispatcher->dispatch(OrderPayedEvent::NAME, $event);
        }
    }
}
