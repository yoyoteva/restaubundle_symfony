<?php
/**
 * Created by PhpStorm.
 * User: joffr
 * Date: 22/01/2019
 * Time: 12:02
 */

namespace App\Service;

use App\Entity\User;
use Psr\Log\LoggerInterface;

class RhService
{
    private $logger;
    private $api_endpoint;

    public function __construct(LoggerInterface $logger,string $api_endpoint)
    {
        $this->logger = $logger;
        $this->api_endpoint = $api_endpoint;
    }

    public function getPeople(){
        $json = file_get_contents($this->api_endpoint."?method=people");

        $people = json_decode($json, true);

        $peopleToReturn = [];
        foreach ($people as $member){
            array_push($peopleToReturn,$member);
        }
        return $peopleToReturn;
    }

    public function getDayTeam($date){

        $json = file_get_contents($this->api_endpoint."?method=planning&date=".$date);

        $planning = json_decode($json,true);

        $team = [];
        foreach ($planning['midi'] as $member) {
            array_push($team,$member['firstname']." ".$member['lastname']);
        }

        return $team;
    }

    public function setOrderPayed($order){
        $url = $this->api_endpoint.
            "?method=order&order=".$order->getId().
            "&amount=".$order->getAmount().
            "&server=".$order->getOwner()->getUsername();

        return json_decode(file_get_contents($url));
    }



    public function convertInUser($member)
    {
        $user = new User();
        $user->setUsername($member->id);
        $user->setFirstname($member->firstname);
        $user->setLastname($member->lastname);
        $user->setEmail($member->email);
        $user->setJobtitle($member->jobtitle);
        $user->setEnabled(true);

        return $user;
    }

}
