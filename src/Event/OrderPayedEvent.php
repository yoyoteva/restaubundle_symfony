<?php
/**
 * Created by PhpStorm.
 * User: joffr
 * Date: 04/02/2019
 * Time: 21:29
 */

namespace App\Event;

use App\Entity\Order;

class OrderPayedEvent
{
    public const NAME = 'order.payed';

    protected $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function getOrderPayed()
    {
        return $this->order;
    }
}
