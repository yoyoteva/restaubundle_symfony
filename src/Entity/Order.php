<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrderRepository")
 * @ORM\Table("Commande")
 */
class Order
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\Column(type="datetime")
     */
    private $datetime_enregistrement;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="orders")
     */
    private $User;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Table", inversedBy="orders")
     */
    private $Numero_table;

    /**
     * @ORM\Column(type="float")
     */
    private $Prix;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Status;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Dish")
     */
    private $Plats;

    public function __construct()
    {
        $this->Plats = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDatetimeEnregistrement(): ?\DateTimeInterface
    {
        return $this->datetime_enregistrement;
    }

    public function setDatetimeEnregistrement(\DateTimeInterface $datetime_enregistrement): self
    {
        $this->datetime_enregistrement = $datetime_enregistrement;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->User;
    }

    public function setUser(?User $User): self
    {
        $this->User = $User;

        return $this;
    }

    public function getNumeroTable(): ?Table
    {
        return $this->Numero_table;
    }

    public function setNumeroTable(?Table $Numero_table): self
    {
        $this->Numero_table = $Numero_table;

        return $this;
    }

    public function getPrix(): ?float
    {
        return $this->Prix;
    }

    public function setPrix(float $Prix): self
    {
        $this->Prix = $Prix;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->Status;
    }

    public function setStatus(string $Status): self
    {
        $this->Status = $Status;

        return $this;
    }

    /**
     * @return Collection|Dish[]
     */
    public function getPlats(): Collection
    {
        return $this->Plats;
    }

    public function addPlat(Dish $plat): self
    {
        if (!$this->Plats->contains($plat)) {
            $this->Plats[] = $plat;
        }

        return $this;
    }

    public function removePlat(Dish $plat): self
    {
        if ($this->Plats->contains($plat)) {
            $this->Plats->removeElement($plat);
        }

        return $this;
    }
}
